import { Injectable } from '@nestjs/common';
import  { Course } from './interfaces/course.interface';

@Injectable()
export class CourseService {
    async findAll(): Promise<Course[]>{
        return [
            { 
              id: '100', //id object ในฐานข้อมูล
              number: '01204111',
              title: 'Computer and Programming'
            },
            { 
              id: '213fds', 
              number: '01204211',
              title: 'awertyuiowertfgtbrst'
            },
            { 
              id: '100sdfghjkl', 
              number: '0120411axsdcfvgbhnjm1',
              title: 'Computer andwefrtbrnyteumyr,tu Programming'
            },
          ];
    }
}