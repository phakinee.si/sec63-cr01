import { Controller, Get } from '@nestjs/common';
import { CourseService } from './courses.service';

@Controller('courses')
export class CoursesController {
  constructor(private coursesService: CourseService) {}

  @Get()
  async findAll(): Promise<Course[]> { // any = object ซักอย่างนึง
    return this.coursesService.findAll();
  }
}

